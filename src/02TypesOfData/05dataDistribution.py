# Uniform Distribution
# Flat line on the probabilty
# Every value has a equal chance
#%%
%matplotlib inline

import numpy as np
import matplotlib.pyplot as plt

values = np.random.uniform(-10.0, 10.0, 100000)
plt.hist(values, 50)
plt.show()

# Normal / Gaussian
#%%
from scipy.stats import norm
import matplotlib.pyplot as plt

x = np.arange(-3, 3, 0.001)
plt.plot(x, norm.pdf(x))

#%%
import numpy as np
import matplotlib.pyplot as plt

mu = 5.0
sigma = 2.0
values = np.random.normal(mu, sigma, 10000)
plt.hist(values, 50)
plt.show()

#Exponential / Power Law
#%%
from scipy.stats import expon
import matplotlib.pyplot as plt

x = np.arange(0, 10, 0.001)
plt.plot(x, expon.pdf(x))

#Binomial Probability Mass Function
#Discrete Data
#%%
from scipy.stats import binom
import matplotlib.pyplot as plt

n, p = 10, 0.5
x = np.arange(0, 10, 0.001)
plt.plot(x, binom.pmf(x, n, p))

# Poisson Probability Mass Function
# Example: My website gets on average 500 visits per day. What's the odds of getting 550?
#%%
from scipy.stats import poisson
import matplotlib.pyplot as plt

mu = 500
x = np.arange(400, 600, 0.5)
plt.plot(x, poisson.pmf(x, mu))


# Pop Quiz!
# What's the equivalent of a probability distribution function when using discrete instead of continuous data?
# Binomial Probability Mass Function

