#%%
%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt

vals = np.random.normal(0, 10, 10)

plt.hist(vals, 50)
plt.show()

#%%
np.percentile(vals, 50) #Median

#which point represents the values that are lower than 90% 
#%%
np.percentile(vals, 90) 

#which point represents the values that are lower than 20% 
#%%
np.percentile(vals, 20)