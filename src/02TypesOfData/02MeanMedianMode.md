# Mean, Median and Mode

* [Mean](#mean)
* [Median](#median)
* [Mode](#mode)

***

## Mean
* AKA avarage
* sum / number of samples

Example:
* Number of children in each house on my street: 0,2,3,1,0

MEAN = (0+2+3+1+0)/5

*** 

## Median
* Sort the values, and take the value at the midpoint

Example: 
0,2,3,2,1,0,0,2,0
>> sort it
0,0,0,0,1,2,2,2,3
>> 1

* if you have an even number of samples, take the advarage of the two in the middle
* is less suceptible to outliers than the mean

Example: 
* MEAN household income in the US is $72,641, but the median is only $51,939 - because the MEAN is skewed by a handful of bilionaires
    * MEDIAN better represents the "typical" American in this example

*** 

## Mode
* The most common value in a data set
    * not relevant to continuous numerical data
* Back to our number of kids in each house example:

0,2,3,2,1,0,0,2,0

How many of eache value are there?

0: 4, 1: 1, 2: 3, 3: 1

The MODE is 0 


