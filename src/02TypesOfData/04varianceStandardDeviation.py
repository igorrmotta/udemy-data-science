#%%
%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt

#centered around 100.0
#standard deviation of 20.0
#10,000 data points
incomes = np.random.normal(100.0, 10.0, 10000)

plt.hist(incomes, 50)
plt.show()

#%%
#standard deviation
incomes.std()

#%%
#variance
incomes.var()

