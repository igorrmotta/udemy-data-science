# Flavors of Data

* [Numerical](#numerical)
* [Categorical](#categorical)
* [Ordinal](#ordinal)

***

## Numerical
Represents quantifiable things. 

Example: Heights of people, page load times, stock prices, etc
* Discrete Data
    * Integer based; often counts of some event
        * How many purchases dia a customer make in a year?
        * How many times did I flip "heads"?
* Continuous Data
    * Has an infinite number of possible values
        * How much time did it take for a user to check out?
        * How much rain fell on a given day?

*** 

## Categorical
No numeric meaning. No mathematical meaning.

Example: Gender, yes/no (binary data), Race, State of Residence, Product Category, Political Party, etc

You can assign assign numbers to categories, but they don't have mathematical meaning

***

## Ordinal
A mixture of numerical and categorical. Like a categorical data that has mathematical meaning

Example: movie ratings on a 1-5 scale
* ratings must be 1,2,3,4 or 5
* but these values have mathematical meaning; 1 means it's a worse movie than a 2

***

### Quiz
* How much gas is in your gas tank
    * numerical - continuous
* A rating of your overall health where the choices are 1,2,3 or 4, corresponding to "poor", "moderate", "good" and "excellent"
    * ordinal
* The races of your classmates
    * categorical
* Ages in years
    * numerical - continuous and discrete
* Money spent in a store
    * numerical - continuous