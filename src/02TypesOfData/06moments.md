# Moments
* Quanttitative measures of the shape of a probability desity function
* Mathematically the are a bit hard to wrap your head around
* But intuitively, it's a lot simpler in statistics

### First Moment: `mean`
### Second moment: `variance`
### Third moment: `skew`

# skew
* How 'lopsided' is the distribution?
* A distribution with a longer tail on the left will be skewed left, nad have a negative skew.