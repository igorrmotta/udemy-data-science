# Percentiles
* In a data set, what's the point at which X% of the values are less than that value?
* Example: income distribution
    * 99th percentile: $500,000
        * it means that 99% of the population earns less than $500,000 and only 1% does earn that

