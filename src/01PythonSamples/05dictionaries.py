#%%
#Like a map or hash table in other languages
captains = {}
captains["Enterprise"] = "Kirk"
captains["Enterprise D"] = "Picard"
captains["Deep Space"] = "Sisko"
captains["Voyage"] = "Janeway"

print captains["Voyage"]


print captains.get("Enterprise")

print captains.get("DX")

for ship in captains:
    print ship + ": " + captains[ship]
