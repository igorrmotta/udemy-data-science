#%%
#create a code that creates a list of integers, 
# loops through each element of the list, 
# and only prints out even numbers

for x in range(10):
    if(x % 2 == 0 and x != 0):
        print x, " is even"