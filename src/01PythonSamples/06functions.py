def squareIt(x):
    return x * x

print squareIt(2)

#you can pass functions around as parameters
def doSomething(f, x):
    return f(x)

print doSomething(squareIt, 4)

#lambda functions let you inline simple functions
print doSomething(lambda x: x * x * x, 3)

